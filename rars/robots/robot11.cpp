/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Joshua Neubert <jos.neubert@dhbw-mosbach.de>
 * @version   0.1
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot11
//--------------------------------------------------------------------------

class Robot11 : public Driver
{
public:
    // Konstruktor
    Robot11()
    {
        // Der Name des Robots
        m_sName = "Robot11";
        // Namen der Autorengit
        m_sAuthor = "Joshua Neubert";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oCYAN;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";

    }

    con_vec drive(situation& s) //Pilotenfunktion
    {
        con_vec result = CON_VEC_EMPTY;
        double vc, speed, nex_speed, alpha, alpha_help, to_end, init;
        static double lane;
        double width = s.to_lft + s.to_rgt;

        if(s.starting) //Start
            {
                result.fuel_amount = MAX_FUEL;
            }

        if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc)) //Falls stecken geblieben
        {
            return result;
        }

        if(s.fuel < 10.0) //Boxenstopp
        {
          result.request_pit   = 1;
          result.repair_amount = s.damage;
          result.fuel_amount = MAX_FUEL;
        }

        if(init == 0.0) //Wird nur einmal beim Start ausgeführt
        {
            lane = s.to_lft;
            init++;
        }

        //lane berechnen mit dem Ziel: konstanter Abstand zu Innenwand in Kurve
        if(s.cur_rad > 0.0) //Linkskurve
        {
            lane = DESIRED_DIST_CORNER;
        }
        else if(s.cur_rad < 0.0) //Rechtskurve
        {
             lane = width - DESIRED_DIST_CORNER;
        }

        //alpha_help berechnen
        if(s.cur_rad == 0.0) //Auf einer Geraden
            {
                alpha_help = 0.0;
                if(s.nex_rad > 0.0)
                    speed = max_speed_corner(DESIRED_DIST_CORNER + s.nex_rad);
                else if(s.nex_rad < 0.0)
                    speed = max_speed_corner(DESIRED_DIST_CORNER - s.nex_rad);
                else
                    speed = 250.0;
            }

        else //In einer Kurve
            {
                if(s.nex_rad == 0.0) //Wenn danach Gerade kommt
                {
                    nex_speed = 250.0;
                }

                else //Wenn danach Kurve kommt
                {
                    nex_speed = max_speed_corner(DESIRED_DIST_CORNER + fabs(s.nex_rad));
                }

                speed = max_speed_corner(DESIRED_DIST_CORNER + fabs(s.cur_rad));
                alpha_help = (s.v*s.v/(speed*speed)) * atan(9.0 / speed);

                if(s.cur_rad < 0.0)
                    alpha_help = -alpha_help;
            }

        //alpha berechnen
       alpha = 0.5 * (s.to_lft - lane)/width - 1.1 * s.vn/s.v + alpha_help; //Werte 0.5 und 1.1 von Tutorial übernommen

        //vc berechnen
        if(s.cur_rad == 0.0) //Auf einer Geraden
        {
            if(s.to_end > min_distance_corner(s.v, speed, BRAKE_ACCEL)) //Weit genug von der nächsten Kurve: Vollgas
            {
                vc = 250.0;
            }

            else //Nach Überschreiten der Grenze: Bremsen/Beschleunigen
            {
                    if(s.v > speed)
                        vc = 0.95 * s.v;
                    else
                        vc = 1.05 * s.v;

                    if(s.nex_rad > 0.0)
                        lane = DESIRED_DIST_CORNER;
                    else
                        lane = width - DESIRED_DIST_CORNER;
            }
        }

        else //Auf einer Kurve
        {
            vc = (s.v + speed)/(2*cos(alpha));

            if(s.cur_rad > 0.0)
            {
                to_end = s.to_end * (s.cur_rad + DESIRED_DIST_CORNER);
            }
            else
            {
                to_end = -s.to_end * (s.cur_rad - DESIRED_DIST_CORNER);
            }

            if(to_end <= min_distance_corner(s.v, nex_speed, BRAKE_ACCEL))
            {
                vc = s.v - 5.0;
            }
        }

        result.vc = vc; result.alpha = alpha;
        return result;
    }


    double max_speed_corner(double radius) //Berechnet maximal fahrbare Geschwindigkeit um in der Kurve zu bleiben
    {
        return sqrt(32.2 * FRICTION_C * radius)+5; //Formel berechnet durch Zentripetalkraft-Gesetze + 5fps Toleranz
    }

    double min_distance_corner(double v0, double v1, double a)
    {
        if((v1-v0) > 0.0)
            return 0.0;

        return (v0 + (v1-v0)/2) * (v1-v0)/a;
    }


private:
    const static double FRICTION_C = 1.0;
    const static double BRAKE_ACCEL = -22.0;
    const static double DESIRED_DIST_CORNER = 15.0;

};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot11Instance()
{
    return new Robot11();
}
