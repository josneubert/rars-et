/**
 * Robot für das Software-Projekt
 *
 * @author
 * @version   ET12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <iostream>
#include "car.h"
#include <math.h>
#include <fstream>

//--------------------------------------------------------------------------
//                           N A M E S P A C E
//--------------------------------------------------------------------------

using namespace std;

//--------------------------------------------------------------------------
//                           Class Robot17
//--------------------------------------------------------------------------

class Robot17 : public Driver
{
private:
//--------------------------------------------------------------------------
//                           K O N S T A N T E N
//--------------------------------------------------------------------------

        static const double    LENK_KRAFT = 0.4;
        static const double    DAEMPFUNGSFAKTOR = 0.75;
        static const double    BREMS_KRAFT  = -30.0;

//--------------------------------------------------------------------------
//                           V A R I A B L E N
//--------------------------------------------------------------------------


        double          alpha;
        double          breite;
        double          restweg;
        con_vec         result;
        double          fahrspur;
        double          vc;



//--------------------------------------------------------------------------
//                           M E T H O D E N
//--------------------------------------------------------------------------

    //Bremswegberechnung in Abhängigkeit des folgenden Kurvenradius
    double bremsweg(double geschwindigkeit, double radius, double bremswert);
    //Sollgeschwindigkeitsberechnung in Abhängigkeit des Kurvenradius'
    double sollgeschwindigkeit(double radius);

public:

//--------------------------------------------------------------------------
//                           K O N S T R U K T O R
//--------------------------------------------------------------------------

    Robot17()
    {
        // Der Name des Robots
        m_sName = "Robot17";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

//--------------------------------------------------------------------------
//                           M E T H O D E
//--------------------------------------------------------------------------

    con_vec drive(situation& s);
};

//--------------------------------------------------------------------------
//                           D R I V E
//--------------------------------------------------------------------------

con_vec Robot17::drive(situation& s)
        {
        if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc)) //Im Falle des Verlassens der Fahrspur
        return result;

        breite = s.to_lft + s.to_rgt; //Breite der Fahrbahn


        if(s.cur_rad == 0.0)//Restwegberechnung
            restweg = s.to_end; //Bei Gerade ist s.to_end schon in Fuß
        else
            restweg = fabs(s.cur_rad) * (s.to_end / 2);  //Anteiliger Kurvenumfang vom Innenradius

        //Geschwindigkeit Gerade oder Kurve
        if(s.cur_rad == 0.0) //Gerade
        {
            if(restweg > bremsweg(s.v, s.nex_rad, BREMS_KRAFT)) //Vor Bremspunkt
                vc = s.v + 250.0;                               //Volle Beschleunigung
            else                                                //Nach Bremspunkt
                vc = sollgeschwindigkeit(s.nex_rad);            //Anpassung nächste Kurve
        }
        else //Kurve
        {
            if(s.nex_rad != 0.0 && restweg <= bremsweg(s.v, s.nex_rad, BREMS_KRAFT))    //Nach Bremspunkt
                vc = sollgeschwindigkeit(s.nex_rad);                                    //Anpassung an nächste Kurve
            else                                                                        //Vor Bremspunkt
                vc = sollgeschwindigkeit(s.cur_rad);                                    //Anpassung an aktuelle Kurve
        }

        //Fahrspur einstellen
        if((s.cur_rad == 0.0) && (s.to_end > 0.3 * s.cur_len))                      //Gerade
        {
            fahrspur = breite / 2;                                              //Mitte der Fahrbahn
        }
        else if((s.to_end > 0.15 * s.cur_len) && (s.to_end <= 0.3 * s.cur_len))     //kurz vor Kurve
        {
            fahrspur = 0.5*breite - breite * (s.to_end/s.cur_len);      //Etwas näher an Innenbahn fahren
        }
        else if(s.to_end <= 0.15 * s.cur_len)                                       //direkt vor Rechtskurve
        {
            fahrspur = 0.1*breite - breite * (s.to_end/s.cur_len);      //nahe an Innenfahrbahn fahren
        }
        else if((s.cur_rad != 0.0) && (0.5*s.cur_len <= s.to_end))                  //Kurve erste hälfte
        {
            fahrspur = 0.0*breite-1/(breite*(s.cur_len/1.1*s.to_end));  //Annäherung an Innenbahn bis zum Scheitelpunkt der Kurve
        }
        else if((s.cur_rad != 0.0) && (0.5*s.cur_len > s.to_end))                   //Kurve zweite hälfte
        {
            fahrspur = 0.0*breite-1/(breite*(s.to_end/1.1*s.cur_len));  //Entfernung von Innenbahn ab dem Scheitelpunkt der Kurve
        }

        //Richtung der Kurve bestimmen (fahrspur ist immer für Rechtskurve angegeben)
        if(s.cur_rad < 0.0 || (((s.nex_rad) < 0.0) && (s.cur_rad == 0.0)))  //im Falle einer Linkskurve
        {
            fahrspur = breite - fahrspur;                                           //berechnete fahrspur von der Fahrbahnbreite abziehen
        }
        //Lenkfunktion
        alpha = LENK_KRAFT * (s.to_lft - fahrspur)/breite - DAEMPFUNGSFAKTOR * s.vn/s.v;

        //Rückgabe der Werte für folgende Fahrt
        result.alpha = alpha;
        result.vc = vc;
        return result;
    }

//--------------------------------------------------------------------------
//                           S O L L G E S C H W I N D I G K E I T
//--------------------------------------------------------------------------

    double Robot17::sollgeschwindigkeit(double radius) //Bremswegberechnung mit dem Radius der Kurve
    {
        double geschwindigkeit;
        if(radius < 0.0)
        {
            radius=-radius;
        }
        geschwindigkeit = sqrt(radius * 44);
        return geschwindigkeit;
    }

//--------------------------------------------------------------------------
//                           B R E M S W E G
//--------------------------------------------------------------------------

    double Robot17::bremsweg(double geschwindigkeit, double radius, double bremswert)//Bremswegberechnung in Abhängigkeit des folgenden Kurvenradius
    {
        double geschwindigkeit_diff;
        geschwindigkeit_diff = sollgeschwindigkeit(radius) - geschwindigkeit; //Differenzbildung zwischen aktueller- und Sollgeschwindigkeit
        if(geschwindigkeit_diff > 0.0)
        {
            return 0.0;
        }
        return ((geschwindigkeit + .5 * geschwindigkeit_diff) * geschwindigkeit_diff / bremswert);
    }


/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot17Instance()
{
    return new Robot17();
}
