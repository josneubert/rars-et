/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "car.h"

//--------------------------------------------------------------------------
//                           D E F I N E S
//--------------------------------------------------------------------------

// parameters to tinker with:
// accelerations are in feet/second per second.
// slips are in feet/second
// distances are in feet
const double CORN_MYU   = 1.00;     // lateral g's expected when cornering
const double BRAKE_ACCEL = -33.0;   // acceleration when braking on straight
const double BRAKE_SLIP  = 6.5;     // tire slip when braking
const double BRK_CRV_ACC = -27.0;   // acceleration when braking in curve
const double BRK_CRV_SLIP = 3.5;    // tire slip for braking in curve
const double MARGIN = 10.0;         // target distance from curve's inner rail
const double MARG2 = MARGIN+10.0;   // target for entering the curve
const double ENT_SLOPE = .33;       // slope of entrance path before the curve
const double STEER_GAIN = 1.0;      // gain of steering servo
const double  DAMP_GAIN = 1.2;      // damping of steering servo
const double  BIG_SLIP = 9.0;       // affects the bias of steering servo
const double CURVE_END = 4.0;       // when you are near end of curve, widths
const double TOO_FAST = 1.02;    // a ratio to determine if speed is OK in curve
const double DELTA_LANE = 2.5;   // if collision predicted, change lane by this

//--------------------------------------------------------------------------
//                           Class Robot00
//--------------------------------------------------------------------------

class Robot00 : public Driver
{
public:
    // Konstruktor
    Robot00()
    {
        // Der Name des Robots
        m_sName = "Robot00";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oCYAN;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue"; //JOSH Umändern.
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

  con_vec drive(situation &s)       // This is the robot "driver" function:
  {
    con_vec result = CON_VEC_EMPTY; // This is what is returned.
    double alpha, vc;             // components of result
    double bias;                  // added to servo's alpha result when entering curve
    double speed;                 // target speed for curve (next curve if straightaway)
    double speed_next = 0.0;      // target speed for next curve when in a curve, fps.
    double width;                 // track width, feet
    double to_end;                // distance to end of present segment in feet.
    static double lane;           // target distance from left wall, feet
    static double lane0;          // value of lane during early part of straightaway
    static int rad_was = 0;       // 0, 1, or -1 to indicate type of previous segment
    static double lane_inc = 0.0; // an adjustment to "lane", for passing

    return result;
  }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot00Instance()
{
    return new Robot00();
}
